/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/04 18:05:33 by mafagot           #+#    #+#             */
/*   Updated: 2015/09/04 18:17:29 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			ft_perror_exit(const char *str, int flag)
{
	if (flag)
		perror(str);
	else
		ft_putendl_fd(str, 2);
	//fonction pour fermer la minilibX si ouverte;
	exit(EXIT_FAILURE);
}

char			*ft_rejoin_free(char *s1, char *s2, int flag)
{
	size_t		len1;
	size_t		len2;
	char		*ret;

	if (!s1 || !s2)
		return (s1 ? s1 : s2);
	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	ret = NULL;
	if (!(ret = (char *)malloc(sizeof(char) * (len1 + len2 + 1))))
		ft_perror_exit("malloc()", 1);
	while (*s1)
		*ret++ = *s1++;
	while (*s2)
		*ret++ = *s2++;
	*ret = '\0';
	if (flag == 1 || flag == 3)
		free(s1 - len1);
	if (flag == 2 || flag == 3)
		free(s2 - len2);
	return (ret - len1 - len2);
}