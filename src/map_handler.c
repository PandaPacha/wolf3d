/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_handler.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/04 17:25:40 by mafagot           #+#    #+#             */
/*   Updated: 2015/09/05 14:45:05 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static void			get_dim_map(char *map, t_env *env)
{
	unsigned int	rows;
	unsigned int	lines;

	rows = 0;
	lines = 0;
	while (*map)
	{
		while (*map && *map != '\n')
		{
			while (*map && (*map == ' ' || *map == '\t'))
				map++;
			rows++;
			while (*map && *map != ' ' && *map != '\t')
				map++;
		}
		if (*map == '\n')
		{
			lines++;
			map++;
		}
	}
	if (lines <= 1 && rows <= 1)
		ft_perror_exit("Error: invalid map", 0);
	env->map->rows = rows + 2;
	env->map->lines = lines + 2;
}

static void				populate_map(char *map, t_env *env)
{
	unsigned int	i;
	unsigned int	j;

	i = 1;
	while (*map && i + 1 < env->map->lines)
	{
		j = 1;
		
		while (*map != '\n' && j + 1 < env->map->rows)
		{
			while (*map && (*map == ' ' || *map == '\t'))
				map++;
			env->map->tab[i][j++] = ft_atoi(map);
			while (*map && *map != ' ' && *map != '\t' && *map != '\n')))
				map++;
		}
		i++;
		map++;
	}
}

static void			build_map(char *map, t_env *env)
{
	int				**tab;
	unsigned int	i;
	unsigned int	j;

	tab = NULL;
	if (!(tab = (int **)malloc(sizeof(int *) * env->map->lines)))
		ft_perror_exit("malloc()", 1);
	i = 0;
	while (i < env->map->lines)
	{
		if (!(tab[i] = (int *)malloc(sizeof(int) * env->map->rows)))
			ft_perror_exit("malloc()", 1);
		j = 0;
		tab[i][j++] = 1;
		while (j < env->map->rows - 1)
			tab[i][j++] = 0;
		tab[i][j] = 1;
		i++;
	}
	env->map->tab = tab;
	populate_map(map, env);
}

void				get_map(t_env *env, char *file_map)
{
	int		fd;
	int		check_ret;
	char	*line;
	char	*map_l;

	if ((fd = open(file_map, O_RDONLY)) < 0)
		ft_perror_exit("open()", 1);
	line = NULL;
	map_l = NULL;
	while ((check_ret = get_next_line(fd, &line) > 0)
	{
		if (!map)
			map = line;
		else
			map = ft_rejoin_free(map, line, 3);
	}
	if (check_ret < 0)
		ft_perror_exit("get_next_line(): an error occured", 0);
	free(line);
	if (close(fd))
		ft_perror_exit("close()", 1);
	ft_strlen(map_l) ? get_dim_map(env, map_l) : ft_perror_exit("Map error", 0);
	build_map(map_l, env);
	free(map_l);
}
