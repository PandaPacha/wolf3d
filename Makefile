#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/06/08 11:42:29 by rpatillo          #+#    #+#              #
#    Updated: 2015/09/05 15:45:12 by jsebayhi         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = wolf3d

FLAGS = -Wall -Wextra -Werror

CC = gcc
# LIB = -framework AppKit -framework OpenGL -L minilibx_macos/ -lmlx -L libft -lft
LIB = -L libft -lft
COMP = -I minilibx_macos/ -I libft/ -I inc/

SRC_PATH =	./src/
SRC_CF =	main.c \
		#	map_handler.c \

SRC_C = $(addprefix $(SRC_PATH), $(SRC_CF))

HEAD_PATH = ./inc/
HEAD_F = 	wolf3d.h \
		struct_wolf3d.h
HEAD = $(addprefix $(HEAD_PATH), $(HEAD_F))

FILE_OBJ = $(SRC_C:.c=.o)

.PHONY:	all clean fclean re
all: $(NAME)

$(NAME): $(FILE_OBJ)
		make -C ./libft
		make -C ./libft clean
		make -C ./minilibx_macos
		$(CC)  $(HEAD$) $(SRC_C) $(FLAGS) $(COMP) $(LIB)
		@clear
		@echo	""
		@echo	Wolf3d
		@echo	""
		@echo "		Flags :"$(FLAGS)
		@echo	""
		@echo "		Maps are located in maps dir"


clean:
		make -C ./libft/ clean
		make -C ./minilibx_macos/ clean
		rm -Rfv $(FILE_OBJ)
		@clear

fclean: clean
		make -C libft/ fclean
		make -C minilibx_macos/ clean
		rm -Rfv $(NAME) $(FILE_OBJ)
		@clear

re: fclean all
