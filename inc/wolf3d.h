/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/04 20:30:32 by mafagot           #+#    #+#             */
/*   Updated: 2015/09/04 20:57:06 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "struct_wolf3d.h"
# include "mlx.h"
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <fcntl.h>

void		ft_perror_exit(const char *str, int flag);
char		*ft_rejoin_free(char *s1, char *s2, int flag);


#endif