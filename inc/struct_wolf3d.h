/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_wolf3d.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mafagot <mafagot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/04 20:32:04 by mafagot           #+#    #+#             */
/*   Updated: 2015/09/04 21:24:12 by mafagot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_WOLF3D_H
# define STRUCT_WOLF3D_H

typedef struct		s_point
{
	double			xx;
	double			yy;
}					t_point;

typedef struct		s_map
{
	unsigned int	rows;
	unsigned int	lines;

}					t_map;

typedef struct		s_env
{
	void			*img;
	void			*ptr;
	void			*win;
	char			*data;
	char			*dataxpm;
	int				bpp;
	int				sline;
	int				endian;
}					t_env;

typedef				t_truc
{
	t_point			pos;
	
}					s_tru

typedef				t_truc
{
	t_map			*map;
	t_point			*cam;
	t_env			*env;
}					s_truc;

#endif